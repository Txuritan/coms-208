#include <iostream>

int main() {
    // Initialize all variables
    int doorHeight = 0, doorWidth = 0, doorSquareFeet = 0;

    int windowOneHeight = 0, windowOneWidth = 0, windowOneSquareFeet = 0;
    int windowTwoHeight = 0, windowTwoWidth = 0, windowTwoSquareFeet = 0;

    int bookshelfHeight = 0, bookshelfWidth = 0, bookshelfSquareFeet = 0;

    int roomHeight = 0, roomLength = 0, roomWidth = 0, roomSquareFeet = 0;

    int totalSquareFeet = 0;
    double estimatePaint = 0.0;

    // Ask user for input
    std::cout << "Enter door height: ";
    std::cin >> doorHeight;
    std::cout << "Enter door width: ";
    std::cin >> doorWidth;


    std::cout << "Enter first window height: ";
    std::cin >> windowOneHeight;
    std::cout << "Enter first window width: ";
    std::cin >> windowOneWidth;

    std::cout << "Enter second window height: ";
    std::cin >> windowTwoHeight;
    std::cout << "Enter second window width: ";
    std::cin >> windowTwoWidth;


    std::cout << "Enter bookshelf height: ";
    std::cin >> bookshelfHeight;
    std::cout << "Enter bookshelf width: ";
    std::cin >> bookshelfWidth;


    std::cout << "Enter room height: ";
    std::cin >> roomHeight;
    std::cout << "Enter room length: ";
    std::cin >> roomLength;
    std::cout << "Enter room width: ";
    std::cin >> roomWidth;

    // Primary math logic
    // Doors square feet
    doorSquareFeet = doorHeight * doorWidth;

    // Windows square feet
    windowOneSquareFeet = windowOneHeight * windowOneWidth;
    windowTwoSquareFeet = windowTwoHeight * windowTwoWidth;

    // Bookshelfs square feet
    bookshelfSquareFeet = bookshelfHeight * bookshelfWidth;

    // Calculate room square feet
    // horizontal walls * vertical walls
    roomSquareFeet = ((roomHeight * roomWidth) * 2) + ((roomHeight * roomLength) * 2);

    // Subtract blockages from final amount
    totalSquareFeet = roomSquareFeet - (doorSquareFeet + windowOneSquareFeet + windowTwoSquareFeet + bookshelfSquareFeet);

    // Final 
    estimatePaint = static_cast<double>(totalSquareFeet) / 120.0;

    // Print details
    std::cout << "Estimate amount of paint: " << estimatePaint << std::endl;

    return 1;
}
