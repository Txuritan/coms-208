# Chapter 3

## Exercise 4

![exercise-4](exercise-4/screenshot.png "exercise-4")

```cpp
#include <iostream>
#include <iomanip>

int main() {
    double cost;
    double area;

    double bagSize;

    std::cout << std::fixed << std::showpoint << std::setprecision(2);

    std::cout << "Enter the amount of fertilizer, in pounds, in one bag: ";
    std::cin >> bagSize;
    std::cout << std::endl;

    std::cout << "Enter the cost of the " << bagSize << " pound fertilizer bag: ";
    std::cin >> cost;
    std::cout << std::endl;

    std::cout << "Enter the area, in square feet, that can be fertilized by on bag: ";
    std::cin >> area;
    std::cout << std::endl;

    std::cout << "The cost of the fertilizer per pound is: $" << cost / bagSize << std::endl;
    std::cout << "The cost of fertilizing per square foot is: $" << cost / area << std::endl;

    return 0;
}
```

## Exercise 8

![exercise-8](exercise-8/screenshot.png "exercise-8")

```cpp
#include <iostream>
#include <iomanip>

int main() {
    // Initialize variables
    double merchandise = 0.0;
    double salary = 0.0;
    double rent = 0.0;
    double electricity = 0.0;

    // Set number output limits
    std::cout << std::fixed << std::showpoint << std::setprecision(2);

    // Set variables to user input
    std::cout << "Enter merchandise cost: ";
    std::cin >> merchandise;
    std::cout << std::endl;

    std::cout << "Enter salary cost: ";
    std::cin >> salary;
    std::cout << std::endl;

    std::cout << "Enter rent cost: ";
    std::cin >> rent;
    std::cout << std::endl;

    std::cout << "Enter electricity cost: ";
    std::cin >> electricity;
    std::cout << std::endl;

    // Math logic
    double markdown = (merchandise * 0.15) + salary + rent + electricity;
    double profit = merchandise + salary + rent + electricity;

    // Print findings
    std::cout << "The merchandise requires a " << 10.0 - (profit / markdown) << " markup" << std::endl;

    return 0;
}
```

## Exercise 12

![exercise-12](exercise-12/screenshot.png "exercise-12")

```cpp
#include <iostream>
#include <cmath>

int main() {
    // Initialize int
    double carASpeed = 0.0;
    double carADistance = 0.0;
    double carBSpeed = 0.0;
    double carBDistance = 0.0;

    int hours = 0;
    int minutes = 0;

    // User input
    std::cout << "Enter speed of car a: ";
    std::cin >> carASpeed;
    std::cout << std::endl;

    std::cout << "Enter speed of car b: ";
    std::cin >> carBSpeed;
    std::cout << std::endl;

    std::cout << "Enter hours driven for, not including minutes: ";
    std::cin >> hours;
    std::cout << std::endl;

    std::cout << "Enter minutes driven for, not including hours: ";
    std::cin >> minutes;
    std::cout << std::endl;

    // Math
    hours = (minutes / 60) + hours;

    carADistance = carASpeed * hours;
    carBDistance = carBSpeed * hours;

    // Use pythagorean theorem to find the shortest distance
    double shortest = std::sqrt((carADistance * carADistance) + (carBDistance * carBDistance));

    std::cout << "The shortest distance between the two cars is " << shortest << " miles" << std::endl;

    return 0;
}
```
