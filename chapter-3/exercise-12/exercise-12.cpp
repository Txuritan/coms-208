#include <iostream>
#include <cmath>

int main() {
    // Initialize int
    double carASpeed = 0.0;
    double carADistance = 0.0;
    double carBSpeed = 0.0;
    double carBDistance = 0.0;

    int hours = 0;
    int minutes = 0;

    // User input
    std::cout << "Enter speed of car a: ";
    std::cin >> carASpeed;
    std::cout << std::endl;

    std::cout << "Enter speed of car b: ";
    std::cin >> carBSpeed;
    std::cout << std::endl;

    std::cout << "Enter hours driven for, not including minutes: ";
    std::cin >> hours;
    std::cout << std::endl;

    std::cout << "Enter minutes driven for, not including hours: ";
    std::cin >> minutes;
    std::cout << std::endl;

    // Math
    hours = (minutes / 60) + hours;

    carADistance = carASpeed * hours;
    carBDistance = carBSpeed * hours;

    // Use pythagorean theorem to find the shortest distance
    double shortest = std::sqrt((carADistance * carADistance) + (carBDistance * carBDistance));

    std::cout << "The shortest distance between the two cars is " << shortest << " miles" << std::endl;

    return 0;
}
