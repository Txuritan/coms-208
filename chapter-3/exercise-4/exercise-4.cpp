#include <iostream>
#include <iomanip>

int main() {
    double cost;
    double area;

    double bagSize;

    std::cout << std::fixed << std::showpoint << std::setprecision(2);

    std::cout << "Enter the amount of fertilizer, in pounds, in one bag: ";
    std::cin >> bagSize;
    std::cout << std::endl;

    std::cout << "Enter the cost of the " << bagSize << " pound fertilizer bag: ";
    std::cin >> cost;
    std::cout << std::endl;

    std::cout << "Enter the area, in square feet, that can be fertilized by on bag: ";
    std::cin >> area;
    std::cout << std::endl;

    std::cout << "The cost of the fertilizer per pound is: $" << cost / bagSize << std::endl;
    std::cout << "The cost of fertilizing per square foot is: $" << cost / area << std::endl;

    return 0;
}
