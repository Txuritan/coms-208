#include <iostream>
#include <iomanip>

int main() {
    // Initialize variables
    double merchandise = 0.0;
    double salary = 0.0;
    double rent = 0.0;
    double electricity = 0.0;

    // Set number output limits
    std::cout << std::fixed << std::showpoint << std::setprecision(2);

    // Set variables to user input
    std::cout << "Enter merchandise cost: ";
    std::cin >> merchandise;
    std::cout << std::endl;

    std::cout << "Enter salary cost: ";
    std::cin >> salary;
    std::cout << std::endl;

    std::cout << "Enter rent cost: ";
    std::cin >> rent;
    std::cout << std::endl;

    std::cout << "Enter electricity cost: ";
    std::cin >> electricity;
    std::cout << std::endl;

    // Math logic
    double markdown = (merchandise * 0.15) + salary + rent + electricity;
    double profit = merchandise + salary + rent + electricity;

    // Print findings
    std::cout << "The merchandise requires a " << 10.0 - (profit / markdown) << " markup" << std::endl;

    return 0;
}
