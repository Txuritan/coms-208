# Chapter 4

## Exercise 6

![exercise-6](chapter-4/exercise-6/screenshot.png "exercise-6")

```cpp
#include <iostream>
#include <cmath>

// If this isn't allowed, let me know and I'll stop using it
template <typename T> T input(char *msg) {
    T value;

    std::cout << msg;
    std::cin >> value;

    return value;
}

int main() {
    // User input
    int totalCookies = input<int>("Enter total number of cookies: ");
    int cookiesInABox = input<int>("Enter total number of cookies in a box: ");
    int boxesInAContainer = input<int>("Enter total number of boxes in a container: ");

    // Box math (round up to fit all cookies)
    int boxesOfCookies = static_cast<int>(ceil(static_cast<float>(totalCookies) / cookiesInABox));
    int containersOfBoxes = static_cast<int>(ceil(static_cast<float>(boxesOfCookies) / boxesInAContainer));

    // Print output
    std::cout << "To ship the cookies you need " << containersOfBoxes << " containers with " << boxesOfCookies << " in each" << std::endl;
}
```

## Exercise 20

![exercise-20](chapter-4/exercise-20/screenshot.png "exercise-20")

```cpp
#include <iostream>
#include <iomanip>

template <typename T> T input(char *msg) {
    T value;

    std::cout << msg;
    std::cin >> value;

    return value;
}

int main() {
    // Variables for discounts
    double discount30 = 0.30, discount20 = 0.20, discount10 = 0.10;
    double discount = 0.0;

    // Ask user for input
    double rentCost = input<double>("Enter the cost of rent for one room: ");
    int roomsBooked = input<int>("Enter number of rooms booked: ");
    int daysBooked = input<int>("Enter number of days the room is booked: ");
    int salesTax = input<int>("Enter the sales tax (percentage): ");

    // Set base discount according to amount of days
    if (roomsBooked >= 30) {
        discount = discount30;
    } else if (roomsBooked >= 20) {
        discount = discount20;
    } else if (roomsBooked >= 10) {
        discount = discount10;
    }

    if (daysBooked >= 3) {
        discount += 0.05;
    }

    std::cout << std::fixed << std::showpoint << std::setprecision(2);

    // Total cost (including discount)
    double bookingCost = (static_cast<double>(roomsBooked) * rentCost) * daysBooked;

    // Add discount
    bookingCost -= (bookingCost * discount);

    // Cost of each room (without tax)
    double costPerRoom = static_cast<double>(roomsBooked) / bookingCost;

    // Add tax
    double tax = (static_cast<double>(salesTax) / 100.0);
    double totalTax = (bookingCost * tax);
    bookingCost += totalTax;

    std::cout << "Cost per room: $" << costPerRoom << std::endl;
    std::cout << "Discount on each room: " << discount * 100 << "%" << std::endl;
    std::cout << "Number of rooms booked: " << roomsBooked << std::endl;
    std::cout << "Days the rooms are booked: " << daysBooked << std::endl;
    std::cout << "Tax: $" << totalTax << std::endl;
    std::cout << "Total cost: $" << bookingCost << std::endl;
}
```

# Chapter 5

## Exercise 6

![exercise-8](chapter-5/exercise-8/screenshot.png "exercise-8")

```cpp
#include <iostream>

template <typename T> T input(char *msg) {
    T value;

    std::cout << msg;
    std::cin >> value;

    return value;
}

int main() {
    // User input
    int firstNum = input<int>("Enter first number: ");
    int secondNum = input<int>("Enter second number: ");


    std::cout << std::endl << std::endl;


    // Part A
    int oddSum = 0;
    int evenSum = 0;
    int spanNum = firstNum;

    while (spanNum < secondNum) {
        if (spanNum % 2 == 0) {
            evenSum += spanNum;
        } else {
            oddSum += spanNum;

            std::cout << "Odd number: " << spanNum << std::endl;
        }

        spanNum++;
    }

    std::cout << "Squared sum of odd numbers: " << oddSum * oddSum << std::endl;
    std::cout << "Sum of even numbers: " << evenSum << std::endl;


    std::cout << std::endl << std::endl;


    // Part D
    int squareNum = 1;

    while (squareNum < 10) {
        std::cout << "Square of " << squareNum << ": " << squareNum * squareNum << std::endl;

        squareNum++;
    }


    std::cout << std::endl << std::endl;


    // Part F
    char letter = 'A';

    while (letter <= 'Z') {
        std::cout << letter << " ";

        letter++;
    }

    std::cout << std::endl;
}
```

## Exercise 6

![exercise-14](chapter-5/exercise-14/screenshot.png "exercise-14")

```cpp
#include <iostream>

template <typename T> T input(char *msg) {
    T value;

    std::cout << msg;
    std::cin >> value;

    return value;
}

int main() {
    // Ask user for input
    int x = input<int>("Enter a number: ");

    // Variables
    int k = 0;

    int largest = 0;
    int place = 0;

    // Main loop
    std::cout << "Sequence: ";

    // Stop once x is the lowest it can be
    while (x != 1) {
        // Increase k count
        k++;

        // Set largest and its place
        if (x > largest) {
            largest = x;
            place = k;
        }

        std::cout << x << ", ";

        if (x % 2 == 0) {
            // x is even
            x = x / 2;
        } else {
            // x is odd
            x = x * 3 + 1;
        }
    }

    std::cout << x << std::endl;

    // Print final information
    std::cout << "K is " << k << std::endl;
    std::cout << "Largest number is " << largest << " at " << place << std::endl;
}
```

## Exercise 6

![exercise-28](chapter-5/exercise-28/screenshot.png "exercise-28")

```cpp
#include <iostream>

template <typename T> T input(char *msg) {
    T value;

    std::cout << msg;
    std::cin >> value;

    return value;
}

int main() {
    // Ask for user input
    int unitCount = input<int>("Enter number of units: ");
    int unitMaintenance = input<int>("Enter maintenance cost per unit: ");
    int rentIncrease = input<int>("Enter rent increase per vacant unit: ");
    int rentAllUnits = input<int>("Enter rent for all occupied units: ");

    // Final output and comparision variables
    int highestRent = 0;
    int highestVacant = 0;


    // The main math, increases the amount of vacant units as it goes
    for (int vacant = 0; vacant <= unitCount; vacant++) {
        int increasedRent = rentIncrease * vacant;
        int occupiedUnits = unitCount - vacant;

        int maintenanceCost = occupiedUnits * unitMaintenance;
        int rentWithIncrease = rentAllUnits + increasedRent;

        // Basically `((600 + (40 * vacant)) * (50 - vacant)) + ((50 - vacant) * 27)`
        // At least from what I understand (using book's numbers)
        int rent = (rentWithIncrease * occupiedUnits) + maintenanceCost;

        if (rent > highestRent) {
            highestRent = rent;
            highestVacant = vacant;
        } else {
            // Stop the loop as we have reached the highest
            break;
        }
    }

    // Print the final output
    std::cout << "Number of units to be rented: " << (unitCount - highestVacant) << std::endl;
}
```
