#include <iostream>
#include <iomanip>

template <typename T> T input(char *msg) {
    T value;

    std::cout << msg;
    std::cin >> value;

    return value;
}

int main() {
    // Variables for discounts
    double discount30 = 0.30, discount20 = 0.20, discount10 = 0.10;
    double discount = 0.0;

    // Ask user for input
    double rentCost = input<double>("Enter the cost of rent for one room: ");
    int roomsBooked = input<int>("Enter number of rooms booked: ");
    int daysBooked = input<int>("Enter number of days the room is booked: ");
    int salesTax = input<int>("Enter the sales tax (percentage): ");

    // Set base discount according to amount of days
    if (roomsBooked >= 30) {
        discount = discount30;
    } else if (roomsBooked >= 20) {
        discount = discount20;
    } else if (roomsBooked >= 10) {
        discount = discount10;
    }

    if (daysBooked >= 3) {
        discount += 0.05;
    }

    std::cout << std::fixed << std::showpoint << std::setprecision(2);

    // Total cost (including discount)
    double bookingCost = (static_cast<double>(roomsBooked) * rentCost) * daysBooked;

    // Add discount
    bookingCost -= (bookingCost * discount);

    // Cost of each room (without tax)
    double costPerRoom = static_cast<double>(roomsBooked) / bookingCost;

    // Add tax
    double tax = (static_cast<double>(salesTax) / 100.0);
    double totalTax = (bookingCost * tax);
    bookingCost += totalTax;

    std::cout << "Cost per room: $" << costPerRoom << std::endl;
    std::cout << "Discount on each room: " << discount * 100 << "%" << std::endl;
    std::cout << "Number of rooms booked: " << roomsBooked << std::endl;
    std::cout << "Days the rooms are booked: " << daysBooked << std::endl;
    std::cout << "Tax: $" << totalTax << std::endl;
    std::cout << "Total cost: $" << bookingCost << std::endl;
}
