#include <iostream>
#include <cmath>

// If this isn't allowed, let me know and I'll stop using it
template <typename T> T input(char *msg) {
    T value;

    std::cout << msg;
    std::cin >> value;

    return value;
}

int main() {
    // User input
    int totalCookies = input<int>("Enter total number of cookies: ");
    int cookiesInABox = input<int>("Enter total number of cookies in a box: ");
    int boxesInAContainer = input<int>("Enter total number of boxes in a container: ");

    // Box math (round up to fit all cookies)
    int boxesOfCookies = static_cast<int>(ceil(static_cast<float>(totalCookies) / cookiesInABox));
    int containersOfBoxes = static_cast<int>(ceil(static_cast<float>(boxesOfCookies) / boxesInAContainer));

    // Print output
    std::cout << "To ship the cookies you need " << containersOfBoxes << " containers with " << boxesOfCookies << " in each" << std::endl;
}
