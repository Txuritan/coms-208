#include <iostream>

template <typename T> T input(char *msg) {
    T value;

    std::cout << msg;
    std::cin >> value;

    return value;
}

int main() {
    // Ask user for input
    int x = input<int>("Enter a number: ");

    // Variables
    int k = 0;

    int largest = 0;
    int place = 0;

    // Main loop
    std::cout << "Sequence: ";

    // Stop once x is the lowest it can be
    while (x != 1) {
        // Increase k count
        k++;

        // Set largest and its place
        if (x > largest) {
            largest = x;
            place = k;
        }

        std::cout << x << ", ";

        if (x % 2 == 0) {
            // x is even
            x = x / 2;
        } else {
            // x is odd
            x = x * 3 + 1;
        }
    }

    std::cout << x << std::endl;

    // Print final information
    std::cout << "K is " << k << std::endl;
    std::cout << "Largest number is " << largest << " at " << place << std::endl;
}
