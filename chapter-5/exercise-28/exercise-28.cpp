#include <iostream>

template <typename T> T input(char *msg) {
    T value;

    std::cout << msg;
    std::cin >> value;

    return value;
}

int main() {
    // Ask for user input
    int unitCount = input<int>("Enter number of units: ");
    int unitMaintenance = input<int>("Enter maintenance cost per unit: ");
    int rentIncrease = input<int>("Enter rent increase per vacant unit: ");
    int rentAllUnits = input<int>("Enter rent for all occupied units: ");

    // Final output and comparision variables
    int highestRent = 0;
    int highestVacant = 0;


    // The main math, increases the amount of vacant units as it goes
    for (int vacant = 0; vacant <= unitCount; vacant++) {
        int increasedRent = rentIncrease * vacant;
        int occupiedUnits = unitCount - vacant;

        int maintenanceCost = occupiedUnits * unitMaintenance;
        int rentWithIncrease = rentAllUnits + increasedRent;

        // Basically `((600 + (40 * vacant)) * (50 - vacant)) + ((50 - vacant) * 27)`
        // At least from what I understand (using book's numbers)
        int rent = (rentWithIncrease * occupiedUnits) + maintenanceCost;

        if (rent > highestRent) {
            highestRent = rent;
            highestVacant = vacant;
        } else {
            // Stop the loop as we have reached the highest
            break;
        }
    }

    // Print the final output
    std::cout << "Number of units to be rented: " << (unitCount - highestVacant) << std::endl;
}
