#include <iostream>

template <typename T> T input(char *msg) {
    T value;

    std::cout << msg;
    std::cin >> value;

    return value;
}

int main() {
    // User input
    int firstNum = input<int>("Enter first number: ");
    int secondNum = input<int>("Enter second number: ");


    std::cout << std::endl << std::endl;


    // Part A
    int oddSum = 0;
    int evenSum = 0;
    int spanNum = firstNum;

    while (spanNum < secondNum) {
        if (spanNum % 2 == 0) {
            evenSum += spanNum;
        } else {
            oddSum += spanNum;

            std::cout << "Odd number: " << spanNum << std::endl;
        }

        spanNum++;
    }

    std::cout << "Squared sum of odd numbers: " << oddSum * oddSum << std::endl;
    std::cout << "Sum of even numbers: " << evenSum << std::endl;


    std::cout << std::endl << std::endl;


    // Part D
    int squareNum = 1;

    while (squareNum < 10) {
        std::cout << "Square of " << squareNum << ": " << squareNum * squareNum << std::endl;

        squareNum++;
    }


    std::cout << std::endl << std::endl;


    // Part F
    char letter = 'A';

    while (letter <= 'Z') {
        std::cout << letter << " ";

        letter++;
    }

    std::cout << std::endl;
}
