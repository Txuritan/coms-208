#include <iostream>

template <typename T> T input(char *msg) {
    T value;

    std::cout << msg;
    std::cin >> value;
    std::cout << std::endl;

    return value;
}
